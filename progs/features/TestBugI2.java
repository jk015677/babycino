class TestBugI2 {

    public static void main (String[] a){
        System.out.println(new Test().f());
    }

}

class Test{
     

     // Return an int
    public int f(){
        int result;
        int x;
        
        x = 10 ;
        x++; //x should now be 11

        if (x < 2 && !(x < 1)){ // Equivalent to x = 1
            result = 0;
        } else {
            result = 1;
        }

        return result;
        
       
    }
}
